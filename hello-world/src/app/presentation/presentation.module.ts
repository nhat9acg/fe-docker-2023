import { NgModule } from '@angular/core';
import { ElephantCardListComponent } from './elephant-card-list/elephant-card-list.component';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { DataModule } from '../data/repository/data.module';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    DataModule
  ],
  declarations: [
    ElephantCardListComponent
  ],
  exports: [
    ElephantCardListComponent
  ],
  providers: [
  ]
})
export class PresentationModule { }
