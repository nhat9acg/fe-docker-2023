import { Component } from '@angular/core';
import { ElephantModel } from 'src/app/core/domain/elephant.model';
import { GetAllElephantsUsecase } from 'src/app/core/usecases/get-all-elephants.usecase';

@Component({
  selector: 'app-elephant-card-list',
  templateUrl: './elephant-card-list.component.html',
  styleUrls: ['./elephant-card-list.component.css']
})
export class ElephantCardListComponent {
  // elephants: any;
  elephants: ElephantModel[] = [];
  constructor(private getAllElephants: GetAllElephantsUsecase) {

  }

  updateElephants() {
    this.elephants = [];
    this.getAllElephants.execute().subscribe((value: ElephantModel) => {
      this.elephants.push(value);
    });
  }

  onSelect() {
  }
}
